/*
package adding

import "errors"

type Payload []Message

// The Event type is used to define the possible outcomes from the "adding actor"
type Event int

// The Event values
const (
	// Done: finished processing successfully
	Done Event = iota
	// MsgAlreadyExists: given message is a duplicate of an existing one
	MsgAlreadyExists
	// Failed: processing did not finish successfully
	Failed
)

// Conversion of an event value into a descriptive string
func (e Event) Description() string {
	if e == Done {
		return "Done"
	}
	if e == MsgAlreadyExists {
		return "Duplicate message"
	}
	if e == Failed {
		return "Failed"
	}
	return "Unknown result"
}

// ErrDuplicate is used when trying to add a message which already exists
var ErrDuplicate = errors.New("message already exists")

// The Service interface, providing message adding operations
type Service interface {
	AddMessage(...Message)
	AddSampleMessages(Payload) <-chan Event
}

// The Repository interface, providing access to message repository
type Repository interface {
	AddMessage(Message) error
}

// The service struct depends on a Repository for messages
type service struct {
	mR Repository
}

// The service initializer, used to inject the Repository
func NewService(r Repository) Service {
	return &service{r}
}

func (s *service) AddMessage(m ...Message) {
	for _, msg := range m {
		_ = s.mR.AddMessage(msg) // TODO: error handling
	}
}

func (s *service) AddSampleMessages(data Payload) <-chan Event {
	results := make(chan Event)

	go func() {
		defer close(results)

		for _, b := range data {
			err := s.mR.AddMessage(b)
			if err != nil {
				if err == ErrDuplicate {
					// forgive the naughty error type checking above...
					results <- MsgAlreadyExists
					continue
				}
				results <- Failed
				continue
			}

			results <- Done
		}
	}()

	return results
}
*/

