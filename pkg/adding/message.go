package adding

import "time"

// Message defines the properties of a message to be added
type Message struct {
	Title string `json: title`
	Desc string `json: desc`
	Created time.Time `json: created`
}