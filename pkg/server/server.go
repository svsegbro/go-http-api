package server

/*
// A server usually looks something like this
type server struct {
	db     *someDatabase
	router *someRouter
	email  EmailSender
}
*/

/*
// The router will match specific url's to handler functions.
// If the handler has a dependency, make sure to pass it in as an argument.
func (s *server) handleSomething(dependency dependencyType) http.HandlerFunc {
	// If an endpoint has its own request and response types, usually they’re only useful for that particular handler.
	// If that’s the case, you can define them inside the function.
	type request struct {
        Name string
    }
    type response struct {

	// The prepareThing is called only once, so you can use it to do one-time per-handler initialisation, and then use the thing in the handler.
	thing := prepareThing()

	// The handler function below does not actually handle the requests, it returns a function that does.
    return func(w http.ResponseWriter, r *http.Request) {
        // use thing
		// use dependencyType
    }
}
*/

/*
// Middleware are just Go functions
// Middleware functions take an http.HandlerFunc & return a new one that can run code before and/or after calling the original handler
// — or it can decide not to call the original handler at all.
func (s *server) adminOnly(h http.HandlerFunc) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        if !currentUser(r).IsAdmin {
            http.NotFound(w, r)
            return
        }
        h(w, r)
    }
}
*/
