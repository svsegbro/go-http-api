# Go HTTP API 
Prototype of a HTTP API in Golang. 
  
# Code structure
Inspiration from:
* https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1  
* https://github.com/marcusolsson/goddd
* https://github.com/katzien/go-structure-examples
* https://medium.com/statuscode/how-i-write-go-http-services-after-seven-years-37c208122831

