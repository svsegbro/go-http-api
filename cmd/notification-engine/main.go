package notification_engine

import (
	"log"
	"os"
	"gopkg.in/natefinch/lumberjack.v2"

)

func main() {
	// Configure Logging
	LOG_FILE_LOCATION := os.Getenv("LOG_FILE_LOCATION")
	if LOG_FILE_LOCATION != "" {
		log.SetOutput(&lumberjack.Logger{
			Filename:   LOG_FILE_LOCATION,
			MaxSize:    500, // megabytes
			MaxBackups: 3,
			MaxAge:     28,   //days
			Compress:   true, // disabled by default
		})
	}
	log.Println("Starting Notification API")

	// Subscribe to the topic "visitor"

	// Fire up workers

	/*
	   curl -X POST -s \
	   --form-string "app_key=xxx" \
	   --form-string "app_secret=yyy" \
	   --form-string "target_type=channel" \
	   --form-string "target_alias=zzz" \
	   --form-string "content=Someone at the door [23h59]." \
	   https://api.pushed.co/1/push
	*/
}