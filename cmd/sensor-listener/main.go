package main

import (
	"../../pkg/controllers"
	"github.com/gorilla/mux"
	"net/http"
	//"gopkg.in/natefinch/lumberjack.v2"
)

func main() {
	// setup Logging
	/*LogFileLocation := os.Getenv("LOG_FILE_LOCATION")
	if LogFileLocation != "" {
		log.SetOutput(&lumberjack.Logger{
			Filename:   LogFileLocation,
			MaxSize:    500, // megabytes
			MaxBackups: 3,
			MaxAge:     28,   //days
			Compress:   true, // disabled by default
		})
	}
	log.Println("Starting Sensors API")*/

	// define routes & connect to corresponding controller
	router := mux.NewRouter().StrictSlash(true)
	sub := router.PathPrefix("/api/v1").Subrouter()
	sub.Methods("GET").Path("/messages").HandlerFunc(controllers.GetMessages)
	sub.Methods("POST").Path("/messages").HandlerFunc(controllers.SaveMessage)

	// start the server
	//log.Fatal(http.ListenAndServe(":1337", router)) // TODO: port setting as input parameter (flag)
	http.ListenAndServe(":1337", router) // TODO: port setting as input parameter (flag)
}