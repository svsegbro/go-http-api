# Dockerfile inspired by  https://github.com/raidboss-io/go-docker-compose/blob/master/Dockerfile

###################################################
# Plain env as basement and for local development #
###################################################
FROM golang:alpine as env

# Support for TLS ( https://hackernoon.com/alpine-docker-image-with-secured-communication-ssl-tls-go-restful-api-128eb6b54f1f )
RUN apk add --no-cache ca-certificates
#COPY ./mycert.crt /usr/local/share/ca-certificates/mycert.crt #copy local certificate to desired folder inside the image to be built.
#RUN update-ca-certificates #update new cert into corresponding folder

# Support for the Delve debugger
RUN apk add --no-cache git
RUN go get github.com/derekparker/delve/cmd/dlv

##########################################################
# Prepare a build container with all dependencies inside #
##########################################################
FROM env as builder

#COPY ./app/ ./
#RUN go build -o /go/bin/main main.go

LABEL maintainer="Sven Van Segbroeck <svsegbro@gmail.com>"
WORKDIR /app
COPY ../go.mod go.sum ./
RUN go mod download
COPY ../sensor-listener .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

###########################################
# Create clean container with binary only #
###########################################
FROM alpine as exec

RUN apk add --update bash ca-certificates

WORKDIR /app
COPY --from=builder /go/bin/main ./

ENTRYPOINT ["/app/main"]


RUN apk --no-cache add ca-certificates
WORKDIR /root/

# create log dir
ARG LOG_DIR=/app/logs
RUN mkdir -p ${LOG_DIR}
# set environment var for log file location (our application logs will be written to this location)
ENV LOG_FILE_LOCATION=${LOG_DIR}/app.log

COPY --from=builder /app/main .
VOLUME [${LOG_DIR}]

CMD ["./main"]






